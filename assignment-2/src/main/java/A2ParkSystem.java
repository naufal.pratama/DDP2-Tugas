import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import cage.Cage;
import cage.CageSystem;

import java.util.ArrayList;
import java.util.Scanner;

public class A2ParkSystem {

    // Attributes
    private static CageSystem hamsterCages = new CageSystem(true);
    private static CageSystem catCages = new CageSystem(true);
    private static CageSystem eagleCages = new CageSystem(false);
    private static CageSystem lionCages = new CageSystem(false);
    private static CageSystem parrotCages = new CageSystem(true);

    // Main method
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String rawInput;
        int catAmount, eagleAmount, hamsterAmount, lionAmount, parrotAmount;
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        // Cat input
        System.out.print("cat: ");
        rawInput = input.nextLine();
        catAmount = Integer.parseInt(rawInput);
        // If there are cat(s)
        if (catAmount > 0) {
            System.out.println("Provide the information of cat(s):");
            rawInput = input.nextLine();
            String[] catList = rawInput.split(",");
            for (String item : catList) {
                String catName = item.split("\\|")[0];
                String lengthStr = item.split("\\|")[1];
                int catLength = Integer.parseInt(lengthStr);
                Cat cat = new Cat(catName, catLength);
                catCages.addCage(new Cage(cat));
            }
        }

        // Lion input
        System.out.print("lion: ");
        rawInput = input.nextLine();
        lionAmount = Integer.parseInt(rawInput);
        // If there are lion(s)
        if (lionAmount > 0) {
            System.out.println("Provide the information of lion(s):");
            rawInput = input.nextLine();
            String[] lionList = rawInput.split(",");
            for (String item : lionList) {
                String lionName = item.split("\\|")[0];
                String lengthStr = item.split("\\|")[1];
                int lionLength = Integer.parseInt(lengthStr);
                Lion lion = new Lion(lionName, lionLength);
                lionCages.addCage(new Cage(lion));
            }
        }

        // Eagle input
        System.out.print("eagle: ");
        rawInput = input.nextLine();
        eagleAmount = Integer.parseInt(rawInput);
        // If there are eagle(s)
        if (eagleAmount > 0) {
            System.out.println("Provide the information of eagle(s):");
            rawInput = input.nextLine();
            String[] eagleList = rawInput.split(",");
            for (String item : eagleList) {
                String eagleName = item.split("\\|")[0];
                String lengthStr = item.split("\\|")[1];
                int eagleLength = Integer.parseInt(lengthStr);
                Eagle eagle = new Eagle(eagleName, eagleLength);
                eagleCages.addCage(new Cage(eagle));
            }
        }

        // Parrot input
        System.out.print("parrot: ");
        rawInput = input.nextLine();
        parrotAmount = Integer.parseInt(rawInput);
        // If there are parrot(s)
        if (parrotAmount > 0) {
            System.out.println("Provide the information of parrot(s):");
            rawInput = input.nextLine();
            String[] parrotList = rawInput.split(",");
            for (String item : parrotList) {
                String parrotName = item.split("\\|")[0];
                String lengthStr = item.split("\\|")[1];
                int parrotLength = Integer.parseInt(lengthStr);
                Parrot parrot = new Parrot(parrotName, parrotLength);
                parrotCages.addCage(new Cage(parrot));
            }
        }

        // Hamster input
        System.out.print("hamster: ");
        rawInput = input.nextLine();
        hamsterAmount = Integer.parseInt(rawInput);
        // If there are hamster(s)
        if (hamsterAmount > 0) {
            System.out.println("Provide the information of hamster(s):");
            rawInput = input.nextLine();
            String[] hamsterList = rawInput.split(",");
            for (String item : hamsterList) {
                String hamsterName = item.split("\\|")[0];
                String lengthStr = item.split("\\|")[1];
                int hamsterLength = Integer.parseInt(lengthStr);
                Hamster hamster = new Hamster(hamsterName, hamsterLength);
                hamsterCages.addCage(new Cage(hamster));
            }
        }

        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");

        // Cage Arranging System
        System.out.println("Cage arrangement:");
        if (catAmount > 0) {
            catCages.firstAssignment();
            catCages.rearrange();
        }
        if (lionAmount > 0) {
            lionCages.firstAssignment();
            lionCages.rearrange();
        }
        if (eagleAmount > 0) {
            eagleCages.firstAssignment();
            eagleCages.rearrange();
        }
        if (parrotAmount > 0) {
            parrotCages.firstAssignment();
            parrotCages.rearrange();
        }
        if (hamsterAmount > 0) {
            hamsterCages.firstAssignment();
            hamsterCages.rearrange();
        }

        System.out.println("NUMBER OF ANIMALS:");
        System.out.println("cat:" + catAmount);
        System.out.println("lion:" + lionAmount);
        System.out.println("parrot:" + parrotAmount);
        System.out.println("eagle:" + eagleAmount);
        System.out.println("hamster:" + hamsterAmount);
        System.out.println("\n=============================================");

        // Asking input until exit command
        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            rawInput = input.nextLine();
            int animalId = Integer.parseInt(rawInput);

            // Each animals operations
            if (animalId == 1) {

                System.out.print("Mention the name of cat you want to visit: ");
                String catName = input.nextLine();
                // Check whether the cat exists
                Cat cat = (Cat) catCages.checkAnimal(catName);
                if (cat != null) {
                    System.out.println("You are visiting " + catName +
                            " (cat) now, what would you like to do?");
                    System.out.println("1: Brush the fur 2: Cuddle");
                    rawInput = input.nextLine();
                    int operation = Integer.parseInt(rawInput);
                    if (operation == 1) {
                        System.out.println("Time to clean " + catName + "\'s fur");
                        System.out.println(catName + " makes a voice: " +
                                cat.makeSound(operation));
                    } else if (operation == 2) {
                        System.out.println(catName + " makes a voice: " +
                                cat.makeSound(operation));
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                }

            } else if (animalId == 2) {

                System.out.print("Mention the name of eagle you want to visit: ");
                String eagleName = input.nextLine();
                // Check whether eagle exist
                Eagle eagle = (Eagle) eagleCages.checkAnimal(eagleName);
                if (eagle != null) {
                    System.out.println("You are visiting " + eagleName +
                            " (eagle) now, what would you like to do?");
                    System.out.println("1: Order to fly");
                    rawInput = input.nextLine();
                    int operation = Integer.parseInt(rawInput);
                    if (operation == 1) {
                        System.out.println(eagleName + " makes a voice: " + eagle.makeSound());
                        System.out.println("You hurt!");
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no eagle with that name! Back to the office!\n");
                }

            } else if (animalId == 3) {

                System.out.print("Mention the name of hamster you want to visit: ");
                String hamsterName = input.nextLine();
                // Check whether hamster exist
                Hamster hamster = (Hamster) hamsterCages.checkAnimal(hamsterName);
                if (hamster != null) {
                    System.out.println("You are visiting " + hamsterName +
                            " (hamster) now, what would you like to do?");
                    System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                    rawInput = input.nextLine();
                    int operation = Integer.parseInt(rawInput);
                    if (operation == 1 || operation == 2) {
                        System.out.println(hamsterName + " makes a voice: " +
                                hamster.makeSound(operation));
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no hamster with that name! Back to the office!\n");
                }

            } else if (animalId == 4) {

                System.out.print("Mention the name of parrot you want to visit: ");
                String parrotName = input.nextLine();
                // Check whether parrot exist
                Parrot parrot = (Parrot) parrotCages.checkAnimal(parrotName);
                if (parrot != null) {
                    System.out.println("You are visiting " + parrotName +
                            " (parrot) now, what would you like to do?");
                    System.out.println("1: Order to fly 2: Do conversation");
                    rawInput = input.nextLine();
                    int operation = Integer.parseInt(rawInput);
                    if (operation == 1) {
                        String sentence = "FLYYYY.....";
                        System.out.println("Parrot " + parrotName + " flies!");
                        System.out.println(parrotName + " makes a voice: " +
                                parrot.makeSound(sentence));
                    } else if (operation == 2) {
                        System.out.print("You say: ");
                        String sentence = input.nextLine();
                        System.out.println(parrotName + " says: " + parrot.makeSound(sentence));
                    } else {
                        String sentence = "HM?";
                        System.out.println(parrotName + " says: " + parrot.makeSound(sentence));
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no parrot with that name! Back to the office!\n");
                }

            } else if (animalId == 5) {

                System.out.print("Mention the name of lion you want to visit: ");
                String lionName = input.nextLine();
                // Check whether lion exist
                Lion lion = (Lion) lionCages.checkAnimal(lionName);
                if (lion != null) {
                    System.out.println("You are visiting " + lionName +
                            " (lion) now, what would you like to do?");
                    System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                    rawInput = input.nextLine();
                    int operation = Integer.parseInt(rawInput);
                    if (operation == 1) {
                        System.out.println("Lion is hunting..");
                    } else if (operation == 2) {
                        System.out.println("Clean the lion\'s mane..");
                    }
                    if (1 <= operation && operation <= 3) {
                        System.out.println(lionName + " makes a voice: " +
                                lion.makeSound(operation));
                    } else {
                        System.out.println("You do nothing...");
                    }
                    System.out.println("Back to the office!\n");
                } else {
                    System.out.println("There is no lion with that name! Back to the office!\n");
                }

            } else if (animalId == 99) {
                System.out.println("Thank you for visiting Javari Park!");
                break;
            } else {
                System.out.println("Command not found");
            }
        }

    }

}