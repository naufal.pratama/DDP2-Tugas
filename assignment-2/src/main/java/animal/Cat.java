package animal;

import java.util.Random;

public class Cat extends Animal {

    // Attributes
    private static final String cuddleSounds[] = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

    // Constructor
    public Cat(String name, int length) {
        super(name, length, "cat");
    }

    // Method to make sound
    public String makeSound(int operation) {
        if (operation == 1) {
            return "Nyaaan...";
        } else if (operation == 2) {
            Random rand = new Random();
            int index = rand.nextInt(4);
            return cuddleSounds[index];
        } else {
            return "";
        }
    }

}