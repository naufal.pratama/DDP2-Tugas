package animal;

public class Lion extends Animal {

    // Constructor
    public Lion(String name, int length) {
        super(name, length, "lion");
    }

    // Method to make sound
    public String makeSound(int operation) {
        if (operation == 1) {
            return "Err...!";
        } else if (operation == 2) {
            return "Hauhhmm!";
        } else if (operation == 3) {
            return "HAUHHMM!";
        }
        return "";
    }

}