package animal;

public class Animal {

    // Attributes
    private String name;
    private int length;
    private boolean isTame;
    private String animalType;

    // Constructor
    public Animal(String name, int length, String animalType) {
        this.name = name;
        this.length = length;
        this.animalType = animalType;
        if (animalType.equals("eagle") || animalType.equals("lion")) {
            this.isTame = false;
        } else {
            this.isTame = true;
        }
    }

    // Setters and Getters
    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }

    public boolean isTame() {
        return isTame;
    }

    public String getAnimalType() {
        return animalType;
    }

}