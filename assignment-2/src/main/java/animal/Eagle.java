package animal;

public class Eagle extends Animal {

    // Constructor
    public Eagle(String name, int length) {
        super(name, length, "eagle");
    }

    // Method to make sound
    public String makeSound() {
        return "Kwaakk....";
    }

}