package animal;

public class Hamster extends Animal {

    // Constructor
    public Hamster(String name, int length) {
        super(name, length, "hamster");
    }

    // Method to make sound
    public String makeSound(int operation) {
        if (operation == 1) {
            return "Ngkkrit.. Ngkkrrriiit";
        } else if (operation == 2) {
            return "Trrr... Trrr...";
        } else {
            return "";
        }
    }

}