package animal;

public class Parrot extends Animal {

    // Constructor
    public Parrot(String name, int length) {
        super(name, length, "parrot");
    }

    // Method to make sound
    public String makeSound(String sentence) {
        return sentence.toUpperCase();
    }

}