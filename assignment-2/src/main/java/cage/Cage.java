package cage;

import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;

public class Cage {

    // Attributes
    private Animal animal;
    private int length;
    private int width;
    private char cageType;
    private boolean isIndoors;
    private String description;

    // Constructor
    public Cage(Animal newAnimal) {
        this.animal = newAnimal;
        int animalLength = animal.getLength();
        if (animal.isTame()) {
            if (animalLength < 45) {
                this.cageType = 'A';
                this.length = 60;
                this.width = 60;
            } else if (45 <= animalLength && animalLength <= 60) {
                this.cageType = 'B';
                this.length = 60;
                this.width = 90;
            } else {
                this.cageType = 'C';
                this.length = 60;
                this.width = 120;
            }
        } else {
            if (animalLength < 75) {
                this.cageType = 'A';
                this.length = 120;
                this.width = 120;
            } else if (75 <= animalLength && animalLength <= 90) {
                this.cageType = 'B';
                this.length = 120;
                this.width = 150;
            } else {
                this.cageType = 'C';
                this.length = 120;
                this.width = 180;
            }
        }
        this.description = String.format("%s (%d - %c)", animal.getName(), animal.getLength(),
                cageType);
    }

    // Setters and getters
    public Animal getAnimal() {
        return animal;
    }

    public boolean hasAnimal(String animalName) {
        return animalName.equals(animal.getName());
    }

    public String getDescription() {
        return description;
    }

}