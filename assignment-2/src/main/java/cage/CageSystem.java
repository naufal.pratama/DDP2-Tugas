package cage;

import animal.Animal;
import cage.Cage;

import java.util.ArrayList;

public class CageSystem {

    // Attributes
    private ArrayList<Cage> levelOne;
    private ArrayList<Cage> levelTwo;
    private ArrayList<Cage> levelThree;
    private boolean isIndoors;

    // Constructor
    public CageSystem(boolean isIndoors) {
        this.isIndoors = isIndoors;
        this.levelOne = new ArrayList<Cage>();
        this.levelTwo = new ArrayList<Cage>();
        this.levelThree = new ArrayList<Cage>();
    }

    // Setter and Getter
    public ArrayList<Cage> getLevelOne() {
        return levelOne;
    }

    public ArrayList<Cage> getLevelTwo() {
        return levelTwo;
    }

    public ArrayList<Cage> getLevelThree() {
        return levelThree;
    }

    // Method to add animals
    public void addCage(Cage cage) {
        levelOne.add(cage);
    }

    // Method to check animal existance
    public Animal checkAnimal(String animalName) {
        for (Cage cage : levelOne) {
            if (cage.hasAnimal(animalName)) return cage.getAnimal();
        }
        for (Cage cage : levelTwo) {
            if (cage.hasAnimal(animalName)) return cage.getAnimal();
        }
        for (Cage cage : levelThree) {
            if (cage.hasAnimal(animalName)) return cage.getAnimal();
        }
        return null;
    }

    // Method to print cages information
    public void printCages() {
        System.out.print("level 3:");
        for (Cage cage : levelThree) {
            System.out.print(" " + cage.getDescription() + ",");
        }
        System.out.println();
        System.out.print("level 2:");
        for (Cage cage : levelTwo) {
            System.out.print(" " + cage.getDescription() + ",");
        }
        System.out.println();
        System.out.print("level 1:");
        for (Cage cage : levelOne) {
            System.out.print(" " + cage.getDescription() + ",");
        }
        System.out.println();
    }

    // Method for first assignment
    public void firstAssignment() {
        CageSystem.spreadCages(this);
        System.out.println("location: " + (isIndoors ? "indoor" : "outdoor"));
        this.printCages();
        System.out.println();
    }

    // Method for arranging
    public void rearrange() {
        CageSystem.rotateLevels(this);
        CageSystem.reverseCages(this);
        System.out.println("After rearrangement..");
        this.printCages();
        System.out.println();
    }

    // Static method to make the animals spread equally
    public static void spreadCages(CageSystem cages) {
        ArrayList<Cage> levelOne = cages.getLevelOne();
        ArrayList<Cage> levelTwo = cages.getLevelTwo();
        ArrayList<Cage> levelThree = cages.getLevelThree();
        ArrayList<Cage> tempList = new ArrayList<Cage>();
        for (Cage cage : levelOne) {
            tempList.add(cage);
        }
        levelOne.clear();
        for (Cage cage : levelTwo) {
            tempList.add(cage);
        }
        levelTwo.clear();
        for (Cage cage : levelThree) {
            tempList.add(cage);
        }
        levelThree.clear();
        int amount = tempList.size();
        int amountOne = amount / 3 + (amount % 3 > 0 ? 1 : 0);
        int amountTwo = amount / 3 + (amount % 3 > 1 ? 1 : 0);
        int amountThree = amount / 3;
        for (Cage cage : tempList) {
            if (levelOne.size() < amountOne) {
                levelOne.add(cage);
            } else if (levelTwo.size() < amountTwo) {
                levelTwo.add(cage);
            } else {
                levelThree.add(cage);
            }
        }
        tempList.clear();
    }

    // Static method to rotate the levels
    public static void rotateLevels(CageSystem cages) {
        ArrayList<Cage> levelOne = cages.getLevelOne();
        ArrayList<Cage> levelTwo = cages.getLevelTwo();
        ArrayList<Cage> levelThree = cages.getLevelThree();
        ArrayList<Cage> tempList = new ArrayList<Cage>();
        for (Cage cage : levelOne) tempList.add(cage);
        levelOne.clear();
        for (Cage cage : levelThree) levelOne.add(cage);
        levelThree.clear();
        for (Cage cage : levelTwo) levelThree.add(cage);
        levelTwo.clear();
        for (Cage cage : tempList) levelTwo.add(cage);
        tempList.clear();
    }

    // Static method to reverse the order
    public static void reverseCages(CageSystem cages) {
        ArrayList<Cage> levelOne = cages.getLevelOne();
        ArrayList<Cage> levelTwo = cages.getLevelTwo();
        ArrayList<Cage> levelThree = cages.getLevelThree();
        ArrayList<Cage> tempList = new ArrayList<Cage>();
        for (int anId = levelOne.size() - 1; anId >= 0; anId--) {
            tempList.add(levelOne.get(anId));
        }
        levelOne.clear();
        for (Cage cage : tempList) {
            levelOne.add(cage);
        }
        levelOne = tempList;
        tempList.clear();
        for (int anId = levelTwo.size() - 1; anId >= 0; anId--) {
            tempList.add(levelTwo.get(anId));
        }
        levelTwo.clear();
        for (Cage cage : tempList) {
            levelTwo.add(cage);
        }
        levelTwo = tempList;
        tempList.clear();
        for (int anId = levelThree.size() - 1; anId >= 0; anId--) {
            tempList.add(levelThree.get(anId));
        }
        levelThree.clear();
        for (Cage cage : tempList) {
            levelThree.add(cage);
        }
        levelThree = tempList;
        tempList.clear();
    }

}