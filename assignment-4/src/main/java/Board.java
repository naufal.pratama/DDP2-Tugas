import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Board extends JFrame{

    // Attributes
    private final int N_PAIRS = 18;
    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private JLabel labelCounter;
    private int counter;
    private Container pane;

    /**
     * The constructor of Class Board
     * @param name, title of the window
     */
    public Board(String name) {
        super(name);

        // Init Cards
        counter = 0;
        cards = new ArrayList<Card>();
        List<Card> cardList = new ArrayList<Card>();
        List<Integer> cardValues = new ArrayList<Integer>();
        for (int i = 0; i < N_PAIRS; i++) {
            cardValues.add(i + 1);
            cardValues.add(i + 1);
        }
        Collections.shuffle(cardValues);
        for (int val : cardValues) {
            Card c = new Card("", val);
            try {
                Image img = ImageIO.read(getClass().getResource("Assets/zoo.png"));
                ImageIcon imgIcon = new ImageIcon(img);
                Icon resized = resizeIcon(imgIcon, 128, 128);
                c.setIcon(resized);
            } catch (Exception ex) {
                System.out.println("File not found!");
                System.exit(0);
            }
            c.setMargin(new Insets(0, 0, 0, 0));
            c.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    selectedCard = c;
                    doTurn();
                }
            });
            cardList.add(c);
        }
        cards = cardList;

        // Init timer
        t = new Timer(500, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });
        t.setRepeats(false);

        // Init the grid for placing cards
        pane = getContentPane();
        JPanel cardGrid = new JPanel();
        cardGrid.setLayout(new GridLayout(6,6));
        for (Card c : cards) {
            cardGrid.add(c);
        }
        JPanel controlGrid = new JPanel();
        controlGrid.setLayout(new GridLayout(0,1));

        // Init the grid of control below the cards
        labelCounter = new JLabel(String.format("Tries: %d", counter) );
        controlGrid.add(labelCounter);
        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                resetAll();
            }
        });
        controlGrid.add(resetButton);

        pane.add(cardGrid, BorderLayout.NORTH);
        pane.add(new JSeparator(), BorderLayout.CENTER);
        pane.add(controlGrid, BorderLayout.SOUTH);
    }

    /**
     * Method to rescale the icons used in the game
     * @param icon the Icon before resized;
     * @param rW the resozed width
     * @param rH the resized height
     * @return
     */
    private static Icon resizeIcon(ImageIcon icon, int rW, int rH) {
        Image img = icon.getImage();
        Image resized = img.getScaledInstance(rW, rH,  java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resized);
    }

    /**
     * Method to do turns
     */
    public void doTurn() {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            try {
                String fileName = "Assets/animal" + String.valueOf(c1.getId()) + ".png";
                Image img = ImageIO.read(getClass().getResource(fileName));
                ImageIcon imgIcon = new ImageIcon(img);
                Icon resized = resizeIcon(imgIcon, 128, 128);
                c1.setIcon(resized);
            } catch (Exception ex) {
                System.out.println("File not found!");
                System.exit(0);
            }
        }
        if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            try {
                String fileName = "Assets/animal" + String.valueOf(c2.getId()) + ".png";
                Image img = ImageIO.read(getClass().getResource(fileName));
                ImageIcon imgIcon = new ImageIcon(img);
                Icon resized = resizeIcon(imgIcon, 128, 128);
                c2.setIcon(resized);
            } catch (Exception ex) {
                System.out.println("File not found!");
                System.exit(0);
            }
            t.start();
        }
    }

    /**
     * Method to reset all cards
     */
    public void resetAll() {
        Collections.shuffle(cards);
        pane = getContentPane();
        JPanel cardGrid = new JPanel();
        cardGrid.setLayout(new GridLayout(6,6));
        for (Card c : cards) {
            cardGrid.add(c);
            c.setMatched(false);
            c.setEnabled(true);
            try {
                String fileName = "Assets/zoo.png";
                Image img = ImageIO.read(getClass().getResource(fileName));
                ImageIcon imgIcon = new ImageIcon(img);
                Icon resized = resizeIcon(imgIcon, 128, 128);
                c.setIcon(resized);
            } catch (Exception ex) {
                System.out.println("File not found!");
                System.exit(0);
            }
        }
        pane.add(cardGrid, BorderLayout.NORTH);
        counter = 0;
        labelCounter.setText("Tries: 0");
        c1 = null;
        c2 = null;
    }

    /**
     * Method to check and reset cards after two of them are opened
     */
    public void checkCards() {
        if (c1.getId() == c2.getId()){
            c1.setEnabled(false);
            c2.setEnabled(false);
            c1.setMatched(true);
            c2.setMatched(true);
        } else {
            try {
                String fileName = "Assets/zoo.png";
                Image img = ImageIO.read(getClass().getResource(fileName));
                ImageIcon imgIcon = new ImageIcon(img);
                Icon resized = resizeIcon(imgIcon, 128, 128);
                c1.setIcon(resized);
                String fileName2 = "Assets/zoo.png";
                ImageIO.read(getClass().getResource(fileName2));
                imgIcon = new ImageIcon(img);
                resized = resizeIcon(imgIcon, 128, 128);
                c2.setIcon(resized);
            } catch (Exception ex) {
                System.out.println("File not found!");
                System.exit(0);
            }
        }
        c1 = null;
        c2 = null;
        counter++;
        labelCounter.setText(String.format("Tries: %d",counter) );
        if (this.isGameWon()){
            String message = String.format("You've won after %d tries", counter);
            JOptionPane.showMessageDialog(this, message);
            System.exit(0);
        }
    }

    /**
     * Method to check whether the game has been won or not
     * @return the boolean that shows the game has been won
     */
    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }

}