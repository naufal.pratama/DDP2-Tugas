import javax.swing.JButton;

public class Card extends JButton{
    private int id;
    private boolean matched = false;

    public Card(String name, int id) {
        super(name);
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
    }
}