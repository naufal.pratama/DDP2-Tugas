import java.awt.Dimension;
import javax.swing.JFrame;


public class Game{
    public static void main(String[] args){
        Board b = new Board("Memory Match");
        b.setPreferredSize(new Dimension(128 * 6, 128 * 7));
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }
}