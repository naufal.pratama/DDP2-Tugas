package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Naufal Pratama Tanansyah
 */
public class AnimalCategoriesReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalCategoriesReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        String[] mamList = {"Hamster", "Lion", "Cat", "Whale"};
        String[] avList = {"Parrot", "Eagle"};
        long cnt = 3;
        boolean cekMam, cekAv, cekRep;
        cekMam = true;
        cekAv = true;
        cekRep = true;
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (anRec[1].equalsIgnoreCase("mammals")) {
                boolean tmp = false;
                for (int i = 0; i < 4; i++) {
                    if (anRec[0].equalsIgnoreCase(mamList[i])) tmp = true;
                }
                if (!tmp) cekMam = false;
            }
            if (anRec[1].equalsIgnoreCase("aves")) {
                boolean tmp = false;
                for (int i = 0; i < 2; i++) {
                    if (anRec[0].equalsIgnoreCase(avList[i])) tmp = true;
                }
                if (!tmp) cekAv = false;
            }
            if (anRec[1].equalsIgnoreCase("reptiles")) {
                boolean tmp = false;
                if (anRec[0].equalsIgnoreCase("snake")) tmp = true;
                if (!tmp) cekAv = false;
            }
        }
        if (!cekAv) cnt--;
        if (!cekMam) cnt--;
        if (!cekRep) cnt--;
        return cnt;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long cnt = 3 - countValidRecords();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!anRec[1].equalsIgnoreCase("mammals")) {
                if (!anRec[1].equalsIgnoreCase("aves")) {
                    if (!anRec[1].equalsIgnoreCase("reptiles")) {
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidSections() {
        List<String> strList = this.getLines();
        long cnt = 3;
        boolean cekMam, cekAv, cekRep;
        cekMam = true;
        cekAv = true;
        cekRep = true;
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (anRec[2].equalsIgnoreCase("Explore the Mammals")) {
                if (!anRec[1].equalsIgnoreCase("mammals")) cekMam = false;
            }
            if (anRec[2].equalsIgnoreCase("World of Aves")) {
                if (!anRec[1].equalsIgnoreCase("aves")) cekAv = false;
            }
            if (anRec[2].equalsIgnoreCase("Reptillian Kingdom")) {
                if (!anRec[1].equalsIgnoreCase("reptiles")) cekRep = false;
            }
        }
        if (!cekAv) cnt--;
        if (!cekMam) cnt--;
        if (!cekRep) cnt--;
        return cnt;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidSections() {
        long cnt = 3 - countValidSections();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!anRec[2].equalsIgnoreCase("Explore the Mammals")) {
                if (!anRec[2].equalsIgnoreCase("World of Aves")) {
                    if (!anRec[2].equalsIgnoreCase("Reptillian Kingdom")) {
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }
}
