package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Lion;
import javari.animal.Mammals;
import javari.animal.Reptiles;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Naufal Pratama Tanansyah
 */
public class AnimalRecordsReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalRecordsReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        int cnt = 0;
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (isValid(anRec)) cnt++;
        }
        return cnt;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        List<String> strList = this.getLines();
        int cnt = 0;
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!isValid(anRec)) cnt++;
        }
        return cnt;
    }

    /**
     * Method to get the arraylist of animals from the csv file
     *
     * @return an arraylist of animals
     */
    public ArrayList<Animal> getList() {
        List<String> strList = this.getLines();
        ArrayList<Animal> retList = new ArrayList<Animal>();
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!isValid(anRec)) continue;
            int id = Integer.parseInt(anRec[0]);
            String type = anRec[1];
            String name = anRec[2];
            Gender gender = Gender.parseGender(anRec[3]);
            double length = Double.parseDouble(anRec[4]);
            double weight = Double.parseDouble(anRec[5]);
            String spec = anRec[6];
            Condition cond = Condition.parseCondition(anRec[7]);
            Animal tempAn;
            if (type.equalsIgnoreCase("Parrot") || type.equalsIgnoreCase("Eagle")) {
                tempAn = new Aves(id, type, name, gender, length, weight, cond, spec);
            } else if (type.equalsIgnoreCase("Snake")) {
                tempAn = new Reptiles(id, type, name, gender, length, weight, cond, spec);
            } else if (type.equalsIgnoreCase("Lion")) {
                tempAn = new Lion(id, type, name, gender, length, weight, cond, spec);
            } else {
                tempAn = new Mammals(id, type, name, gender, length, weight, cond, spec);
            }
            retList.add(tempAn);
        }
        return retList;
    }

    /**
     * method untuk mengecek data dari hewan
     *
     * @param anRec recorded animal dalam bentuk array
     * @return boolean, true jika data valid
     */
    public boolean isValid(String[] anRec) {
        String[] animals = {"Hamster", "Eagle", "Whale", "Cat", "Lion",
                            "Snake", "Parrot"};
        boolean cek1 = false;
        for (int i = 0; i < 7; i++) {
            if (animals[i].equalsIgnoreCase(anRec[1])) cek1 = true;
        }
        if (!cek1) return false;
        if (!(anRec[3].equalsIgnoreCase("male") || anRec[3].equalsIgnoreCase("female"))) {
            return false;
        }
        if (!(anRec[7].equalsIgnoreCase("not healthy") ||
            anRec[7].equalsIgnoreCase("healthy"))) {
            return false;
        }
        return true;
    }

}
