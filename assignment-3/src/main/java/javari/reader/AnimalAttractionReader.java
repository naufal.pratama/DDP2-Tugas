package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Naufal Pratama Tanansyah
 */
public class AnimalAttractionReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalAttractionReader(Path file) throws IOException {
        super (file);
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        String[] cirList = {"Whale", "Lion", "Eagle"};
        String[] danList = {"Parrot", "Snake", "Cat", "Hamster"};
        String[] cntList = {"Parrot", "Whale", "Hamster"};
        String[] codList = {"Snake", "Cat", "Hamster"};
        long cnt = 4;
        boolean cekCir, cekDan, cekCnt, cekCod;
        cekCir = true;
        cekDan = true;
        cekCnt = true;
        cekCod = true;
        for (String item : strList) {
            String[] atRec = item.split(",");
            if (atRec[1].equalsIgnoreCase("Circles of Fires")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(cirList[i])) tmp = true;
                }
                if (!tmp) cekCir = false;
            }
            if (atRec[1].equalsIgnoreCase("Dancing Animals")) {
                boolean tmp = false;
                for (int i = 0; i < 4; i++) {
                    if (atRec[0].equalsIgnoreCase(danList[i])) tmp = true;
                }
                if (!tmp) cekDan = false;
            }
            if (atRec[1].equalsIgnoreCase("Counting Masters")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(cntList[i])) tmp = true;
                }
                if (!tmp) cekCnt = false;
            }
            if (atRec[1].equalsIgnoreCase("Passionate Coders")) {
                boolean tmp = false;
                for (int i = 0; i < 3; i++) {
                    if (atRec[0].equalsIgnoreCase(codList[i])) tmp = true;
                }
                if (!tmp) cekCod = false;
            }
        }
        if (!cekCir) cnt--;
        if (!cekDan) cnt--;
        if (!cekCnt) cnt--;
        if (!cekCod) cnt--;
        return cnt;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long cnt = 4 - countValidRecords();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] atRec = item.split(",");
            if (!atRec[1].equalsIgnoreCase("Circles of Fires")) {
                if (!atRec[1].equalsIgnoreCase("Dancing Animals")) {
                    if (!atRec[1].equalsIgnoreCase("Counting Masters")) {
                        if (!atRec[1].equalsIgnoreCase("Passionate Coders")) {
                            cnt++;
                        }
                    }
                }
            }
        }
        return cnt;
    }
}
