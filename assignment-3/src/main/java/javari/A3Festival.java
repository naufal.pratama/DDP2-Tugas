package javari;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.ExploreTheMammals;
import javari.park.ReptillianKingdom;
import javari.park.RegSystem;
import javari.park.Section;
import javari.park.SelectedAttraction;
import javari.park.WorldOfAves;
import javari.reader.AnimalAttractionReader;
import javari.reader.AnimalCategoriesReader;
import javari.reader.AnimalRecordsReader;
import javari.reader.CsvReader;
import javari.writer.RegistrationWriter;

/**
 * This class is has the main method for the assignment
 *
 * @author Naufal Pratama Tanansyah
 */

public class A3Festival {

    public static void main(String[] args) {

        Section sec1 = new ExploreTheMammals();
        Section sec2 = new WorldOfAves();
        Section sec3 = new ReptillianKingdom();
        RegSystem registration = new RegSystem(1, "");

        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");
        System.out.println("... File not found or incorrect file!\n");
        System.out.print("Please provide the source data path: ");
        String rawInput = input.nextLine();
        Path oPath = Paths.get(rawInput);
        Path path1 = Paths.get(rawInput + "\\animals_categories.csv");
        Path path2 = Paths.get(rawInput + "\\animals_attractions.csv");
        Path path3 = Paths.get(rawInput + "\\animals_records.csv");

        AnimalAttractionReader attReader;
        AnimalCategoriesReader catReader;
        AnimalRecordsReader recReader;
        ArrayList<Animal> anList = new ArrayList<Animal>();

        try {
            catReader = new AnimalCategoriesReader(path1);
            attReader = new AnimalAttractionReader(path2);
            recReader = new AnimalRecordsReader(path3);
            System.out.println("");
            System.out.println("... Loading... Success... System is populating data...\n");

            System.out.format("Found %d valid sections and %d invalid sections\n",
                    catReader.countValidSections(), catReader.countInvalidSections());
            System.out.format("Found %d valid attractions and %d invalid attractions\n",
                    attReader.countValidRecords(), attReader.countInvalidRecords());
            System.out.format("Found %d valid animal categories and %d invalid animal categories\n",
                    catReader.countValidRecords(), catReader.countInvalidRecords());
            System.out.format("Found %d valid animal records and %d invalid animal records\n\n",
                    recReader.countValidRecords(), recReader.countInvalidRecords());

            anList = recReader.getList();
        } catch(IOException e) {
            System.out.println("Error, exitting system...");
            System.exit(0);
        }

        for (Animal curAn : anList) {
            if (curAn.getType().equalsIgnoreCase("Snake")) {
                sec3.addAnimal(curAn);
            } else if (curAn.getType().equalsIgnoreCase("Eagle") ||
                    curAn.getType().equalsIgnoreCase("Parrot")) {
                sec2.addAnimal(curAn);
            } else {
                sec1.addAnimal(curAn);
            }
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("Please answer the questions by typing the number. ");
        System.out.println("Type # if you want to return to the previous menu\n");

        while (true) {
            System.out.println("Javari park has 3 sections:");
            System.out.println("1. Explore the Mammals");
            System.out.println("2. World of Aves");
            System.out.println("3. Reptilian Kingdom");
            System.out.print("Please choose your preferred section (type the number): ");
            rawInput = input.nextLine();
            Attraction selAtt;
            if (rawInput.equals("#")) break;
            int op = Integer.parseInt(rawInput);
            if (op == 1) {
                selAtt = sec1.displayOptions();
            } else if (op == 2) {
                selAtt = sec2.displayOptions();
            } else {
                selAtt = sec3.displayOptions();
            }
            if (selAtt != null) {
                System.out.println("Wow, one more step,");
                System.out.print("please let us know your name :");
                String visitor = input.nextLine();

                System.out.println("");
                System.out.println("Yeay, final check!");
                System.out.println("Here is your data and the attraction you choose:");
                System.out.format("Name : %s\n", visitor);
                System.out.format("Attractions: %s -> %s\n", selAtt.getName(), selAtt.getType());
                System.out.print("With:");
                for (Animal item : selAtt.getPerformers()) {
                    System.out.format(" %s,",item.getName());
                }
                System.out.println("\n");
                System.out.print("Is the data correct? (Y/N): ");
                rawInput = input.nextLine();
                if (rawInput.equalsIgnoreCase("Y")) {
                    registration.setVisitorName(visitor);
                    registration.addSelectedAttraction(selAtt);
                    System.out.print("Thank you for your interest. " +
                            "Would you like to register to other attractions? (Y/N): ");
                    rawInput = input.nextLine();
                    if (rawInput.equalsIgnoreCase("N")) {
                        try {
                            RegistrationWriter.writeJson(registration, oPath);
                        }
                        catch (IOException e){
                            System.out.println("There is something wrong...");
                            System.exit(0);
                        }
                        System.exit(0);
                    }
                }
            }
        }

    }
}