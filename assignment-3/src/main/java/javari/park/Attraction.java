package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;

/**
 * This interface describes expected behaviours for the attraction
 *
 * @author Naufal Pratama Tanansyah
 */
public class Attraction implements SelectedAttraction {

    private String name;
    private String type;
    private List<Animal> performers;

    public Attraction(String name, String type, ArrayList<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the type of animal(s) that performing in this attraction.
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    public List<Animal> getPerformers() {
        return performers;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        performers.add(performer);
        return true;
    }
}
