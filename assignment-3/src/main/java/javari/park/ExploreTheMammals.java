package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * Class for the explore the mammals
 *
 * @author Naufal Pratama Tanansyah
 */

public class ExploreTheMammals extends Section {

    public ExploreTheMammals() {
        super("Explore the Mammals");
    }

    public Attraction displayOptions() {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.println("--Explore the Mammals--");
            System.out.println("1. Hamster");
            System.out.println("2. Lion");
            System.out.println("3. Cat");
            System.out.println("4. Whale");
            System.out.print("Please choose your preferred animals (type the number): ");
            String rawInput = input.nextLine();
            if (rawInput.equals("#")) return null;
            int op = Integer.parseInt(rawInput);
            ArrayList<Animal> performers = new ArrayList<Animal>();
            if (op == 1) {
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Hamster")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Hamster can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Hamster---");
                System.out.println("Attractions by Hamster:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Counting Masters");
                System.out.println("3. Passionate Coders");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                if (op2 == 1) {
                    return new Attraction("Dancing Animals", "Hamster", performers);
                } else if (op2 == 2) {
                    return new Attraction("Counting Masters", "Hamster", performers);
                } else {
                    return new Attraction("Passionate Coders", "Hamster", performers);
                }
            } else if (op == 2) {
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Lion")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Lion can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Lion---");
                System.out.println("Attractions by Lion:");
                System.out.println("1. Circles of Fire");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                return new Attraction("Circles of Fire", "Lion", performers);
            } else if (op == 3) {
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Cat")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Cat can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Cat---");
                System.out.println("Attractions by Cat:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Passionate Coders");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                if (op2 == 1) {
                    return new Attraction("Dancing Animals", "Cat", performers);
                } else {
                    return new Attraction("Passionate Coders", "Cat", performers);
                }
            } else {
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Whale")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Whale can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Whale---");
                System.out.println("Attractions by Whale:");
                System.out.println("1. Circles of Fire");
                System.out.println("2. Counting Masters");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                if (op2 == 1) {
                    return new Attraction("Circles of Fire", "Whale", performers);
                } else {
                    return new Attraction("Counting Masters", "Whale", performers);
                }
            }
        }
    }

}