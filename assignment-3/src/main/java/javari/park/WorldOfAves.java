package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * Class for the world of aves
 *
 * @author Naufal Pratama Tanansyah
 */

public class WorldOfAves extends Section {

    public WorldOfAves() {
        super("World of Aves");
    }

    public Attraction displayOptions() {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.println("--World of Aves--");
            System.out.println("1. Eagle");
            System.out.println("2. Parrot");
            System.out.print("Please choose your preferred animals (type the number): ");
            String rawInput = input.nextLine();
            if (rawInput.equals("#")) return null;
            int op = Integer.parseInt(rawInput);
            if (op == 1) {
                ArrayList<Animal> performers = new ArrayList<Animal>();
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Eagle")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Eagle can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Eagle---");
                System.out.println("Attractions by Eagle:");
                System.out.println("1. Circles of Fire");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                return new Attraction("Circles of Fire", "Eagle", performers);
            } else {
                ArrayList<Animal> performers = new ArrayList<Animal>();
                for (Animal item : animalList) {
                    if (item.getType().equalsIgnoreCase("Parrot")) {
                        if (item.isShowable()) performers.add(item);
                    }
                }
                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Parrot can perform any attraction," +
                            " please choose other animals");
                    continue;
                }
                System.out.println("---Parrot---");
                System.out.println("Attractions by Parrot:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Counting Masters");
                rawInput = input.nextLine();
                if(rawInput.equals("#")) continue;
                int op2 = Integer.parseInt(rawInput);
                if (op2 == 1) {
                    return new Attraction("Dancing Animals", "Parrot", performers);
                } else {
                    return new Attraction("Counting Masters", "Parrot", performers);
                }
            }
        }
    }

}