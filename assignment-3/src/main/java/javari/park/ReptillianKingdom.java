package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * Class for the explore the mammals
 *
 * @author Naufal Pratama Tanansyah
 */

public class ReptillianKingdom extends Section {

    public ReptillianKingdom() {
        super("Reptillian Kingdom");
    }

    public Attraction displayOptions() {
        while (true) {
            Scanner input = new Scanner(System.in);
            System.out.println("--Reptillian Kingdom--");
            System.out.println("1. Snake");
            System.out.print("Please choose your preferred animals (type the number): ");
            String rawInput = input.nextLine();
            if (rawInput.equals("#")) return null;
            ArrayList<Animal> performers = new ArrayList<Animal>();
            for (Animal item : animalList) {
                if (item.isShowable()) performers.add(item);
            }
            if (performers.size() == 0) {
                System.out.println("Unfortunately, no snake can perform any attraction," +
                        " please choose other animals");
                continue;
            }
            System.out.println("---Snake---");
            System.out.println("Attractions by Snake:");
            System.out.println("1. Dancing Animals");
            System.out.println("2. Passsionate coders");
            rawInput = input.nextLine();
            if(rawInput.equals("#")) continue;
            int op = Integer.parseInt(rawInput);
            if (op == 1) {
                return new Attraction("Dancing Animals", "Snake", performers);
            } else {
                return new Attraction("Passionate Coders", "Snake", performers);
            }
        }
    }

}