package javari.park;

import java.util.ArrayList;
import javari.animal.Animal;

/**
 * Class for sections and all the operation in a section
 *
 * @author Naufal Pratama Tanansyah
 */

public abstract class Section {

    protected String name;
    protected ArrayList<Animal> animalList;

    /**
     * Construct the sections in Javari park
     *
     * @param name of the section
     * @param list of animals in the section
     * @param list of animal types in the section
     */
    public Section(String name) {
        this.name = name;
        this.animalList = new ArrayList<Animal>();
    }

    /**
     * To display to options each section have
     *
     * @return true if successfully made a new record
     */
    public abstract Attraction displayOptions();

    /**
     * Method to add animal to the section
     *
     * @param animal
     */
    public void addAnimal(Animal animal) {
        animalList.add(animal);
    }

}