package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.park.SelectedAttraction;

/**
 * This interfaces describes expected behaviours for any type ((abstract)
 * class, interface) that represents the concept of attraction registration
 * done by a visitor in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 *     and describe the changes in the comment block
 */
public class RegSystem implements Registration {

    private int id;
    private String name;
    private List<SelectedAttraction> attractions;

    public RegSystem(int id, String name) {
        this.id = id;
        this.name = name;
        attractions = new ArrayList<SelectedAttraction>();
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    public int getRegistrationId() {
        return this.id;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    public String getVisitorName() {
        return this.name;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return
     */
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return attractions;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return {@code true} if the attraction is successfully added into the
     *     list, {@code false} otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        attractions.add(selected);
        return true;
    }
}
