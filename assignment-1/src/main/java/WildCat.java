public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    // Constructor
    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // Computing BMI
    public double computeMassIndex() {
        // TODO Complete me!
        double lengthSquared = (length * length * 0.0001);
        return weight / (lengthSquared);
    }
}
