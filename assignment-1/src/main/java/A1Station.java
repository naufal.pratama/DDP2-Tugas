import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    private static TrainCar lastCar = null;
    private static int numberOfCars = 0;

    // You can add new variables or methods in this class
    public static void departTrain() {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        lastCar.printCar();
        System.out.print("\n");
        double totalMassIndex = lastCar.computeTotalMassIndex();
        double averageMassIndex = totalMassIndex / numberOfCars;
        System.out.printf("Average mass index of all cats: %.2f\n", averageMassIndex);
        // Checking the average BMI of cats
        String category;
        if (averageMassIndex < 18.5) {
            category = "underweight";
        } else if (averageMassIndex < 25) {
            category = "normal";
        } else if (averageMassIndex < 30) {
            category = "overweight";
        } else {
            category = "obese";
        }
        System.out.printf("In average, the cats in the train are *%s*\n", category);
        lastCar = null;
        numberOfCars = 0;
    }

    public static void main(String[] args) {
        // TODO Complete me!
        Scanner input = new Scanner(System.in);
        String strInput = input.nextLine();
        int n = Integer.parseInt(strInput);
        // For the N cats, do
        for (int i = 0; i < n; i++) {
            strInput = input.nextLine();
            String[] arrInput = strInput.split(",");
            String catName = arrInput[0];
            double catWeight = Double.parseDouble(arrInput[1]);
            double catLength = Double.parseDouble(arrInput[2]);
            WildCat newCat = new WildCat(catName, catWeight, catLength);
            if (lastCar == null) {
                lastCar = new TrainCar(newCat);
            } else {
                TrainCar currentLastCar = lastCar;
                lastCar = new TrainCar(newCat, currentLastCar);
            }
            numberOfCars++;
            // If the weight has reached its threshold
            if (lastCar.computeTotalWeight() > THRESHOLD) {
                departTrain();
            }
        }
        // If there is some remaining car
        if (lastCar != null) {
            departTrain();
        }
    }
}
