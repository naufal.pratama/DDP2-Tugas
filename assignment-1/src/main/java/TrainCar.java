public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    private WildCat cat;
    private TrainCar next;

    public TrainCar(WildCat cat) {
        // TODO Complete me!
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        double totalWeight = EMPTY_WEIGHT + cat.weight;
        if (next != null) {
            totalWeight += next.computeTotalWeight();
        }
        return totalWeight;
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        double totalMassIndex = cat.computeMassIndex();
        if (next != null) {
            totalMassIndex += next.computeTotalMassIndex();
        }
        return totalMassIndex;
    }

    public void printCar() {
        // TODO Complete me!
        if (next == null) {
            System.out.printf("(%s)", cat.name);
        } else {
            System.out.printf("(%s)--", cat.name);
            next.printCar();
        }
    }
}
